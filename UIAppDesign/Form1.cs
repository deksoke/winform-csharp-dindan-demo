﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIAppDesign
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        public Form1()
        {
            InitializeComponent();

            this.StyleManager = msmMain;
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.msmMain.Theme = MetroFramework.MetroThemeStyle.Light;
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            this.msmMain.Theme = MetroFramework.MetroThemeStyle.Dark;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            msmMain.Style = MetroFramework.MetroColorStyle.White;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            msmMain.Style = MetroFramework.MetroColorStyle.Orange;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            msmMain.Style = MetroFramework.MetroColorStyle.Purple;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            msmMain.Style = MetroFramework.MetroColorStyle.Green;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            msmMain.Style = MetroFramework.MetroColorStyle.Blue;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (NORTHWNDEntities db = new NORTHWNDEntities())
            {
                metroComboBox1.DisplayMember = "CategoryName";
                metroComboBox1.ValueMember = "CategoryID";
                metroComboBox1.DataSource = db.Categories.ToList();

                metroComboBox2.DisplayMember = "CategoryName";
                metroComboBox2.ValueMember = "CategoryID";
                metroComboBox2.DataSource = db.Categories.ToList();

                metroDateTime1.Value = DateTime.Now.AddDays(-1);
                metroDateTime2.Value = DateTime.Now.AddDays(-2);

                metroGrid1.DataSource = db.Products.ToList();
            }
        }

        private void metroButton5_Click(object sender, EventArgs e)
        {
            MetroFramework.MetroMessageBox.Show(this, "Message", "Alert", MessageBoxButtons.OK);
        }

        private void metroButton6_Click(object sender, EventArgs e)
        {
            MetroFramework.MetroMessageBox.Show(this, "Message", "Infomation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            MetroFramework.MetroMessageBox.Show(this, "Message", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            MetroFramework.MetroMessageBox.Show(this, "Message", "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
        }

        private void metroButton7_Click(object sender, EventArgs e)
        {
            MetroFramework.MetroMessageBox.Show(this, "Message", "Danger", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
        }

        private void metroButton8_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.StyleManager = this.msmMain;
            f.Show();
        }

        private void metroButton9_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.StyleManager = this.msmMain;
            f.ShowDialog();

            f.Dispose();
        }
    }
}
